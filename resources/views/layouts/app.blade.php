<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>@yield('title')</title>

    <!-- base:css -->
    <link rel="stylesheet" href="vendor/typicons.font/font/typicons.css" />
    <link rel="stylesheet" href="vendor/css/vendor.bundle.base.css" />

    <!-- My Css -->
    <link rel="stylesheet" href="css/app.css" />

    <!-- datatable styles -->
    <link rel=" stylesheet" type="text/css" href="vendor/DataTables/datatables.min.css" />

    <!-- fontawesome -->
    <link href="vendor/fontawesome/css/all.css" rel="stylesheet">

    <!-- bootstrap -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">

    <!-- plugin css for this page -->

    <!-- inject:css -->
    <link rel="stylesheet" href="css/vertical-layout-light/style.css" />

    <!-- endinject -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />


    @yield('css')
</head>

<body>
    @yield('master')
    @yield('auth')

    <!-- jquery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- base:js -->
    <script src=" vendor/js/vendor.bundle.base.js"></script>

    <!-- Plugin js for this page-->

    <!-- inject:js -->
    <script src="js/off-canvas.js"></script>
    <script src="js/hoverable-collapse.js"></script>
    <script src="js/template.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/todolist.js"></script>

    <!-- plugin js for this page -->
    <script src="vendor/progressbar.js/progressbar.min.js"></script>
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- dashboard  -->
    <script src="js/dashboard.js"></script>

    <!-- dashboard  -->
    <!-- <script src="vendor/popper/popper.min.js"></script> -->

    <!-- DataTables -->
    <script type="text/javascript" src="vendor/DataTables/datatables.min.js"></script>

    <!-- bootstrap -->
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>

    @stack('script')

</body>

</html>