@extends ('layouts.app')

@section('master')
<div class="container-scroller">

    <!-- /.navbar -->
    @include('layouts.navbar')
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('layouts.settings-panel')

        <!-- partial:partials/_sidebar.html -->
        @include('layouts.sidebar')

        <div class="main-panel">
            <div class="content-wrapper">
                <!-- content here -->
                @yield('content')
            </div>
            @include('layouts.footer')
        </div>
    </div>
    <!-- partial:partials/_footer.html -->

    @endsection