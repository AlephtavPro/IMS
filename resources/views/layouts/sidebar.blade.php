<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <div class="d-flex sidebar-profile">
                <div class="sidebar-profile-image">
                    <!-- image here -->
                    <!-- <i class="w-100 typcn typcn-user"></i> -->
                    <!-- <img src="images/faces/face29.png" alt="image" /> -->
                    <!-- <span class="sidebar-status-indicator"></span> -->
                </div>
                <div class="sidebar-profile-name">
                    <p class="sidebar-name">{{ Auth::user()->name }}</p>
                    <p class="sidebar-designation">{{ Auth::user()->email }}</p>
                </div>
            </div>
            <!-- Search Bar -->
            <!-- <div class="nav-search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Type to search..." aria-label="search" aria-describedby="search" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="search">
                            <i class="typcn typcn-zoom"></i>
                        </span>
                    </div>
                </div>
            </div> -->
            <p class="sidebar-menu-title">Dash menu</p>
        </li>
        @auth
        @if(( Auth::user()->role == "SystemAdmin"))
        <li class="nav-item">
            <a class="nav-link" href="/dashboard">
                <i class="typcn typcn-home menu-icon"></i>
                <span class="menu-title">Dashboard
                    <!-- new tag -->
                    <!-- <span class="badge badge-primary ml-3">New</span> -->
                </span>
            </a>
        </li>
        @endif
        @if(( Auth::user()->role == "storekeeper") || ( Auth::user()->role == "SystemAdmin") )
        <li class="nav-item">
            <a class="nav-link" href="/inventory">
                <i class="typcn typcn-document-text menu-icon"></i>
                <span class="menu-title">Stock</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/store">
                <i class="typcn typcn-th-small menu-icon"></i>
                <span class="menu-title">Store</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/shop">
                <i class="typcn typcn-shopping-cart menu-icon"></i>
                <span class="menu-title">Shop</span>
            </a>
        </li>
        @endif
        @if(( Auth::user()->role == "sales") || ( Auth::user()->role == "SystemAdmin") )
        <li class="nav-item">
            <a class="nav-link" href="/transaction">
                <i class="typcn typcn-arrow-repeat menu-icon"></i>
                <span class="menu-title">Transaction</span>
            </a>
        </li>
        @endif
        @if(( Auth::user()->role == "sales") || ( Auth::user()->role == "SystemAdmin")||( Auth::user()->role == "storekeeper") )
        <li class="nav-item">
            <a class="nav-link" href="/expenses">
                <i class="typcn typcn-arrow-repeat menu-icon"></i>
                <span class="menu-title">Expenses</span>
            </a>
        </li>
        @endif
        @if(( Auth::user()->role == "SystemAdmin"))
        <li class="nav-item">
            <a class="nav-link" href="/adminManagement">
                <i class="typcn typcn-spanner-outline menu-icon"></i>
                <span class="menu-title">Administration</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/settings">
                <i class="typcn typcn-film menu-icon"></i>
                <span class="menu-title">Setting</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/reports">
                <i class="typcn typcn-film menu-icon"></i>
                <span class="menu-title">reports</span>
            </a>
        </li>
        @endif

    </ul>
    @endauth
    <ul class="sidebar-legend">

        <li>
            <p class="sidebar-menu-title">Other</p>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">#Help</a>
        </li>
    </ul>
</nav>