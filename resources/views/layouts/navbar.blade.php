<!-- partial:partials/_navbar.html -->
<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="
            text-center
            navbar-brand-wrapper
            d-flex
            align-items-center
            justify-content-center
          ">
        <a class="navbar-brand brand-logo" href="#"><img src="assets/images/alephtav-logo.svg" alt="Alephtav Logo" /></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="assets/images/alephtav-logo.svg" alt="Alephtav Logo" /></a>
        <button class="
              navbar-toggler navbar-toggler
              align-self-center
              d-none d-lg-flex
            " type="button" data-toggle="minimize">
            <span class="typcn typcn-th-menu"></span>
        </button>
    </div>
    <div class="
            navbar-menu-wrapper
            d-flex
            align-items-center
            justify-content-end
          ">
          @auth
        <ul class="navbar-nav mr-lg-2">
        @if(( Auth::user()->role == "SystemAdmin"))
            <li class="nav-item d-none d-lg-flex">
                <a class="nav-link @yield('active-dashboard')" href="dashboard">Dashboard</a>
            </li>
            @endif
            @if(( Auth::user()->role == "storekeeper") || ( Auth::user()->role == "SystemAdmin") )
            <li class="nav-item d-none d-lg-flex">
                <a class="nav-link @yield('active-inventory')" href="inventory"> Stock</a>
            </li>
            <li class="nav-item d-none d-lg-flex">
                <a class="nav-link @yield('active-store')" href="store"> Store </a>
            </li>
            <li class="nav-item d-none d-lg-flex">
                <a class="nav-link @yield('active-shop')" href="shop"> Shop </a>
            </li>
            @endif
            @if(( Auth::user()->role == "sales") || ( Auth::user()->role == "SystemAdmin") )
            <li class="nav-item d-none d-lg-flex">
                <a class="nav-link @yield('active-transaction')" href="transaction"> Transaction </a>
            </li>
            @endif
        </ul>
        @endauth
        <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item nav-profile dropdown">
                <a class="nav-link dropdown-toggle pl-0 pr-0" href="#" data-toggle="dropdown" id="profileDropdown">
                    <i class="typcn typcn-user-outline mr-0"></i>
                    <span class="nav-profile-name">{{ Auth::user()->name }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                    <a class="dropdown-item">
                        <i class="typcn typcn-cog text-primary"></i>
                        Settings
                    </a>
                    <a class="dropdown-item" href="http://127.0.0.1:8000/logout" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="typcn typcn-power text-primary"></i>
                        <span class="nav-link">Log Out</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
        <button class="
              navbar-toggler navbar-toggler-right
              d-lg-none
              align-self-center
            " type="button" data-toggle="offcanvas">
            <span class="typcn typcn-th-menu"></span>
        </button>
    </div>
</nav>