 <!-- partial:partials/_footer.html -->
 <footer class="footer">
   <div class="
                d-sm-flex
                justify-content-center justify-content-sm-between
              ">
     <span class="text-center text-sm-left d-block d-sm-inline-block">Copyright ©
       <a href="https://alephtavconsult.com/" target="_blank">alephtavconsult.com</a>
       2021</span>
     <span class="
                  float-none float-sm-right
                  d-block
                  mt-1 mt-sm-0
                  text-center
                ">Inventory Management System<a href="https://alephtavconsult.com/" target="_blank">Alephtav</a> </span>
   </div>
 </footer>