<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Register
</button>

<div class="row">
  <table id="categoryTable" class="display" style="border-radius: 10px;color:black ;background-color: #f5f5f5; width: 100%">
    <thead>
      <tr>
        <th>id</th>
        <th>Item Name</th>
      </tr>
    </thead>
    <tbody>
      @foreach($itemCatagoryall as $cat)
      <tr>
        <td>{{$cat->id}}</td>
        <td>{{$cat->itemCatName}}</td>
        @endforeach
      </tr>
    </tbody>
  </table>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Register Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/categorySave">
          @csrf
          <div class="box-body">
            <div class="form-group">
              <label for="Category">Category</label>
              <input name="Category" class="form-control" placeholder="Enter Category">
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>