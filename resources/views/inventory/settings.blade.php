@extends('layouts.master')
@section('content')
@section('title', 'IMS - Settings')
<div class="col-sm">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="categories-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Categories</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="sub-categories-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Sub-Categories</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="brands-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Brands</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="manufacturers-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Manufacturers</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="banks-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Banks</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="bank-accounts-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Bank Accounts</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="categories-tab">
            hello
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="sub-categories-tab">sub catagories here</div>
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="brands-tab">Brands Here</div>
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="manufacturers-tab">Manufacturers Here</div>
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="banks-tab">Banks Here</div>
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="bank-accounts-tab">Bank Accounts Here</div>
    </div>
</div>
@push('script')
@endpush
@endsection