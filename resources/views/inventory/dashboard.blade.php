@extends('layouts.master')
@section('css')
@endsection
@section('title', 'IMS Dashboard')
@section('active-dashboard', 'active')
@section('content')
<style>
  h1,
  h4 {
    color: white
  }
</style>

<div class="row">
  <div class="col-xl d-flex grid-margin stretch-card ">
    <div class="card h-100">
      <div class="card-body bg-primary">
        <h1 class="display2">{{$itemCount}}</h1>
        <h4>Total Items in stoke</h4>
        <div class="icon">
          <i class="fa fa-archive"></i>
        </div>
      </div>
      <div class="card-footer text-muted">
        <a href="/inventory" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>

  <div class="col-xl d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body bg-secondary">
        <h1 class="display2">{{$shopItemCount}}</h1>
        <h4>Items in Shop</h4>
        <div class="icon">
          <i class="fa fa-archive"></i>
        </div>
      </div>
      <div class="card-footer text-muted">
        <a href="/shop" class="small-box-footer">More info<i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>

  <div class="col-xl d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body bg-success">
        <h1 class="display2">{{$storeItemCount}}</h1>
        <h4>Items in Store </h4>
        <div class="icon">
          <i class="fa fa-archive"></i>
        </div>
      </div>
      <div class="card-footer text-muted">
        <a href="/store" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>

  <div class="col-xl d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body bg-warning">
        <h1 class="display2">{{$LendAmount}}</h1>
        <h4>Lend Amount</h4>
        <div class="icon">
          <i class="fa fa-archive"></i>
        </div>
      </div>
      <div class="card-footer text-muted">
        <a href="/lendPage" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>

  <div class="col-xl d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body bg-info">
        <h1></h1>
        <h4>Number Item sold</h4>
        <div class="icon">
          <i class="fa fa-archive"></i>
        </div>
      </div>
      <div class="card-footer text-muted">
        <a href="/store" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>
</div>
<hr>

<h2>Daily Report</h2>

<div class="row">
  <div class="col-xl d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body bg-info">
        <h1>{{$itemNumber}}</h1>
        <h4>Number of Items Sold Today</h4>
        <div class="icon">
          <i class="fa fa-archive"></i>
        </div>
      </div>
      <div class="card-footer text-muted">
        <a href="GetTransactionDetail" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>

  <div class="col-xl d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body bg-info">
        <h1>{{$totalEarnAmount}} birr</h1>
        <h4>Amount Total Earn Today</h4>
        <div class="icon">
          <i class="fa fa-archive"></i>
        </div>
      </div>
      <div class="card-footer text-muted">
        <a href="GetTransactionDetail" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>

  <div class="col-xl d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body bg-info">
        <h1>{{$totalEarnAmountCash}} birr</h1>
        <h4>Amount Total Earn Today</h4>
        <div class="icon">
          <i class="fa fa-archive"></i>
        </div>
      </div>
      <div class="card-footer text-muted">
        <a href="GetTransactionDetail" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>

  <div class="col-xl d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body bg-info">
        <h1>{{$totalEarnAmountCredit}} birr</h1>
        <h4>Amount Total Earn by credit Today</h4>
        <div class="icon">
          <i class="fa fa-archive"></i>
        </div>
      </div>
      <div class="card-footer text-muted">
        <a href="GetTransactionDetail" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>

  <div class="col-xl d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body bg-info">
        <h1>{{$totalEarnAmountMobileBanking}} birr</h1>
        <h4>Amount Total Earn by Mobile Banking Today</h4>
        <div class="icon">
          <i class="fa fa-archive"></i>
        </div>
      </div>
      <div class="card-footer text-muted">
        <a href="GetTransactionDetail" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>
</div>



@push('script')
<script type="text/javascript" src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function() {
    $('#myTable').DataTable();
  });

  $.fn.digits = function() {
    return this.each(function() {
      $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    })
  }
  $("span.numbers").digits();
</script>
@endpush


@endsection