@extends('layouts.master')
@section('content')
@section('title', 'IMS - Settings')
<div class="col-lg">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="sales-tab" data-toggle="tab" href="#sales" role="tab" aria-controls="home" aria-selected="true">Sales</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="purchases-tab" data-toggle="tab" href="#purchase" role="tab" aria-controls="profile" aria-selected="false">Purchase</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="expense-tab" data-toggle="tab" href="#expenses" role="tab" aria-controls="contact" aria-selected="false">Expense</a>
        </li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="sales" role="tabpanel" aria-labelledby="sales-tab">
            @include('inventory.report.sales')
        </div>
        <div class="tab-pane fade" id="purchase" role="tabpanel" aria-labelledby="purchase-tab">
            New New
        </div>
        <div class="tab-pane fade" id="expenses" role="tabpanel" aria-labelledby="expense-tab">Brands Here</div>
    </div>
</div>
@push('script')
@endpush
@endsection