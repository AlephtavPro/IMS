@extends('layouts.master')
@section('content')
@section('title', 'IMS - Transaction')
@section('active-transaction', 'active')

<div class="row">
    @include('FlashMessage.flashMessage')
    <div class="container form-inline form-group mb-3">
        <button onclick=toggleSaveExpenseFormContainer(); class="btn btn-primary" id="newExpenseFormbutton">New Expense</button>
    </div>
    <br>
    <div class="container" id="saveExpenseFormContainer" style="display:none">
        <!--- Create Transaction form-->
        <form method="post" action="saveExpense" id="expensesForm">
            @csrf
            <div class="row">
                <div class="col-sm-4 form-group-sm">
                    <label for="reasonOfPayment">Reason/Purpose</label>
                    <select name="paymentReason" class="form-control checkField" id="paymentReason" required>
                        <optgroup label="Meal">
                            <option value="Breakfast">Breakfast</option>
                            <option value="Lunch">Lunch </option>
                            <option value="Diner"> Diner</option>
                        </optgroup>
                        <optgroup label="Loading Unloading">
                            <option value="Loading">Loading</option>
                            <option value="Unloading">Unloading </option>
                            <option value="loadingUnloading"> Loadin UnLoading</option>
                        </optgroup>
                        <option id="" value="other">Other</option>
                    </select>
                </div>

                <div class="col-sm-4 form-group-sm" id="otherReason" style="display: none;">
                    <label for="otherReasonType">Reason</label>
                    <input name="otherReasonType" type="text" id="" class="form-control" placeholder="Enter the Reason">
                </div>

                <div class="col-sm-4 form-group-sm">
                    <label for="otherReasonType">Payee</label>
                    <input name="lbourName" type="text" id="labourName" class="form-control" placeholder="Enter Name Of labour" required>
                </div>

                <div class="col-sm-4 form-group-sm">
                    <label for="otherReasonType">Amount</label>
                    <input name="expenseAmount" type="number" id="amountPaid" class="form-control" placeholder="Enter Amount Paid" required>
                </div>

                <div class="col-sm-4 form-group-sm">
                    <label for="otherReasonType">Paid Date</label>
                    <input name="paidDate" type="date" id="dateOfPayment" class="form-control" placeholder="" required>
                </div>

                <div class="col-sm-4 form-group-sm">
                    <label for="Payment Type">Payment Type<span class="requiredStar"> *</span></label>
                    <select name="paymentType" class="form-control checkField" id="paymentType" required>
                        <option value="" disable selected>Please select type</option>
                        <option value="Cash">Cash(on Hand)</option>
                        <option value="Bank">Bank Transfer(include Cheque)</option>
                    </select>
                </div>

                <div class="col-sm-4 form-group-sm">
                    <label for="labourCount">Lbour Count</label>
                    <input name="labourCount" type="number" id="labourCount" class="form-control" placeholder="" required>
                </div>

                <div class="col-sm-4 form-group-sm">
                    <label for="otherReasonType">Payer</label>
                    <input name="payer" type="text" id="payer" class="form-control" placeholder="Please insert payer name" required>
                </div>
            </div>
            <!-- buttons -->
            <div class="col-sm form-group-sm mb-3">
                <br>
                <button type="submit" class="btn btn-success" id="">Save</button>
            </div>
        </form>
    </div>
</div>

<!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////Table Of Tran /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////!-->

<div class="col-sm mt-3" id="itemsListDiv">
    <table id="expenseTable" class="row-border" style="border-radius: 10px;color:black ;background-color: #f5f5f5; width: 100%">
        <thead>
            <tr>
                <th>Payment Reason</th>
                <th>Payee</th>
                <th>Labour Count </th>
                <th>Payment Type</th>
                <th>Paid Amount</th>
                <th>Paid Date</th>
                <th>Payer</th>
            </tr>
        </thead>
        <tbody>
            @foreach($expenseList as $expense)
            <tr>
                <td>{{$expense->paymentReason}}</td>
                <td>{{$expense->lbourName}}</td>
                <td>{{$expense->labourCount}}</td>
                <td>{{$expense->paymentType}}</td>
                <td>{{$expense->expenseAmount}}</td>
                <td>{{$expense->paidDate}}</td>
                <td>{{$expense->payer}}</td>
                @endforeach
            </tr>
        </tbody>
    </table>
</div>

<!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////End Of TRAN/////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-->
@push('script')

<script type="text/javascript">
    //switcher
    function toggleSaveExpenseFormContainer() {
        if ($('#saveExpenseFormContainer').is(":hidden")) {
            $('#saveExpenseFormContainer').show();
        } else {
            $('#saveExpenseFormContainer').hide();
        }
    }

    function resetExpenseForm() {
        form.reset()
        return false;
    };

    var table = $('#expenseTable').DataTable({
        columnDefs: [{
            targets: -1,
            className: 'row-border'
        }],
        "order": [
            [0, "desc"]
        ]
    })

    $('#paymentReason').change(function() {
        var value = $(this).val();
        if (value == 'other') {
            $('#otherReason').show
        } else
            $('#otherReason').hide
    });
</script>
@endpush
@endsection