<div class="col-sm">
    <hr>
    <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
            <a class="nav-link active" href="#cash" data-toggle="tab">Cash</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#credit" data-toggle="tab">Credit</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#mobile" data-toggle="tab">Mobile</a>
        </li>
    </ul>
    <div class="col-xs-9">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="cash">
                @include('inventory.report.sale.cash')
            </div>
            <div class="tab-pane" id="credit">
                @include('inventory.report.sale.creditSales')
            </div>
            <div class="tab-pane" id="mobile">
                @include('inventory.report.sale.mobileBanking')
            </div>
        </div>
    </div>
</div>