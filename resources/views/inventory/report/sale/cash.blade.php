<!-- Create Item Button -->
<h2>Purchase Item By Cash </h2>
<hr>
<div id="itemsListDiv" class="col-md">
    <table id="cashTable" class="row-border" style="border-radius: 10px;color:black ;background-color: #6677ef; width: 100%">
        <thead>
            <tr>
                <th>Item Code</th>
                <th>item Name</th>
                <th>Item Buy Price </th>
                <th>Item Sell Price </th>
                <th>Description </th>
                <th>Item In Shop</th>
                <th>Item IN Store </th>
                <th>Customer</th>
                <th>Transaction Date</th>
                <th>Done By</th>
            </tr>
        </thead>
        <tbody>
            @foreach($cashTransaction as $trans)
            <tr>
                <td>{{$trans->refNumber}}</td>
                <td>{{$trans->itemName}}</td>
                <td>
                    <p>{{$trans->itemCatName}}</p>
                    <p>{{$trans->itemSubCatName}}</p>
                </td>
                <td>{{$trans->quantity}}</td>
                <td> {{$trans->unitPrice}}</td>
                <td>{{$trans->amount}}</td>
                <td>{{$trans->discount}}</td>
                <td>
                    <p>Name : {{$trans->custName}}</p>
                    <p>Phone : {{$trans->custPhone}}</p>
                    <p>TIN : {{$trans->TinNumber}}</p>
                </td>
                <td>{{$trans->created_at}}</td>
                <td>{{$trans->name}}</td>
                @endforeach
            </tr>
        </tbody>
    </table>
    <!--- End of item list div-->
</div>
@push('script')
<script>
    $(document).ready(function() {
        $('#cashTable').DataTable();
    });
</script>
@endpush