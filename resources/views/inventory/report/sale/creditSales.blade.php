<!-- Create Item Button -->
<h2>Credit Sale</h2>
<hr>
<!--- Item list div-->
<div class="col-md" id="itemsListDiv">
    <table id="creditTable" class="row-border" style="border-radius: 10px;color:black ;background-color: #6677ef; width: 100%">
        <thead>
            <tr>
                <th>ref Number</th>
                <th>item Name</th>
                <th>Catagory </th>
                <th>Quantity </th>
                <th>Unit Price </th>
                <th>Amount</th>
                <th>Discount </th>
                <th>Customer</th>
                <th>Transaction Date</th>
                <th>Done By</th>
            </tr>
        </thead>
        <tbody>
            @foreach($creditTransaction as $trans)
            <tr>
                <td>{{$trans->refNumber}}</td>
                <td>{{$trans->itemName}}</td>
                <td>
                    <p>{{$trans->itemCatName}}</p>
                    <p>{{$trans->itemSubCatName}}</p>
                </td>
                <td>{{$trans->quantity}}</td>
                <td> {{$trans->unitPrice}}</td>
                <td>{{$trans->amount}}</td>
                <td>{{$trans->discount}}</td>
                <td>
                    <p>Name : {{$trans->custName}}</p>
                    <p>Phone : {{$trans->custPhone}}</p>
                    <p>TIN : {{$trans->TinNumber}}</p>
                </td>
                <td>{{$trans->created_at}}</td>
                <td>{{$trans->name}}</td>

                @endforeach
            </tr>
        </tbody>
    </table>
</div>
<!--- End of item list div-->

@push('script')
<script>
    $(document).ready(function() {
        $('#creditTable').DataTable();
    });
</script>
@endpush