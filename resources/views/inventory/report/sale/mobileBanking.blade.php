<h2>Mobile Banking Sale</h2>
<hr>
<div class="col-sm" id="itemsListDiv">
    <table id="mobileTable" class="row-border" style="border-radius: 10px;color:black ;background-color: #6677ef; width: 100%">
        <thead>
            <tr>
                <th>ref Number</th>
                <th>item Name</th>
                <th>Catagory </th>
                <th>Quantity </th>
                <th>Unit Price </th>
                <th>Amount</th>
                <th>Discount </th>
                <th>Payment Mode</th>
                <th>Customer</th>
                <th>Transaction Date</th>

                <th>Done By</th>

            </tr>
        </thead>
        <tbody>
            @foreach($mobileTransaction as $trans)
            <tr>
                <td>{{$trans->refNumber}}</td>
                <td>{{$trans->itemName}}</td>
                <td>
                    <p>{{$trans->itemCatName}}</p>
                    <p>{{$trans->itemSubCatName}}</p>
                </td>
                <td>{{$trans->quantity}}</td>
                <td> {{$trans->unitPrice}}</td>
                <td>{{$trans->detailAmount}}</td>
                <td>{{$trans->discount}}</td>
                <td>
                    <p><span style="font-size:14px;font-style: italic;color: blue">Bank :</span> {{$trans->bankName}}</p>
                    <p><span style="font-size:14px;font-style: italic;color: blue">Acount Holder :</span> {{$trans->AcHolder}}</p>
                    <p><span style="font-size:14px;font-style: italic;color: blue">Account Number:</span> {{$trans->AccNumber}}</p>
                </td>
                <td>
                    <p>Name : {{$trans->custName}}</p>
                    <p>Phone : {{$trans->custPhone}}</p>
                    <p>TIN : {{$trans->TinNumber}}</p>
                </td>
                <td>{{$trans->created_at}}</td>
                <td>{{$trans->userName}}</td>

                @endforeach
            </tr>
        </tbody>

    </table>
</div>
<!--- End of item list div-->



@push('script')
<script>
    $(document).ready(function() {
        $('#mobileTable').DataTable();
    });
</script>
@endpush