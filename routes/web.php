<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\user;

//IMS
use App\Http\Controllers\IMS\inventory;
use App\Http\Controllers\IMS\reports;
use App\Http\Controllers\IMS\transaction;
use App\Http\Controllers\IMS\shop;
use App\Http\Controllers\IMS\setting;
use App\Http\Controllers\IMS\dashboard;
use App\Http\Controllers\IMS\expense;

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();


//  IMS routes
Route::group(['middleware' => 'prevent-back-history'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/dashboard', [dashboard::class, 'getDashboard']);

        Route::get('/lendPage', [shop::class, 'getLendItems']);
        Route::get('/shop', [shop::class, 'getItem']);
        Route::get('/getQuantity/{id}', [transaction::class, 'gcoandqty']);
        Route::get('/transaction', [transaction::class, 'getTransForm'])->name('transaction');
        // Route::get('/dashboard', [inventory::class, 'getDashboard']);
        Route::get('/inventory', [inventory::class, 'createInventory'])->name('ItemList');
        Route::get('/store', [inventory::class, 'getStoreItem']);
        Route::post('/createItem', [inventory::class, 'createItem']);
        Route::post('/createTransaction', [transaction::class, 'createNewTransaction']);

        // Route::get('category', function () {
        //     return view('Inventory.category');
        // });

        ROUTE::get('category', [setting::class, 'getCategory']);
        ROUTE::get('manufacturer', [setting::class, 'getManufacture']);
        ROUTE::get('brand', [setting::class, 'getBrand']);
        ROUTE::get('bank', [setting::class, 'getBank']);


        Route::get('bank', function () {
            return view('Inventory.bank');
        });
        Route::get('/subcategory', [setting::class, 'getSubCategory']);
        Route::get('/BankAccount', [setting::class, 'getBankAccount']);
        Route::get('/settings', [setting::class, 'settingsPage']);

        // Route::get('subcategory', function () {
        //     return view('Inventory.subcategory');
        // });
        Route::post('/categorySave', [setting::class, 'createCategory']);

        Route::post('/subCatSave', [setting::class, 'createSubCategory']);
        Route::post('/saveAccount', [setting::class, 'createBankAccount']);
        ///////////////////transfer///////////////////////////
        Route::post('/transferToShop', [inventory::class, 'transferToShop']);
        Route::post('/transferToStore', [inventory::class, 'transferToStore']);
        Route::post('/payCredit', [inventory::class, 'payCreditAmount']);


        ///////////////////Reports///////////////////////////
        Route::get('/reports', [reports::class, 'reportsPage']);


        Route::get('/adminManagement', [user::class, 'adminManagement']);


        Route::get('/GetTransactionDetail', [report::class, 'GetTransactionDetail']);

        Route::get('/cashSales', [report::class, 'getCashSales']);
        Route::get('/creditSales', [report::class, 'getCreditSales']);
        Route::get('/mobileBankingSales', [report::class, 'getMobileBankingSales']);

        Route::get('/purchaseByCash', [report::class, 'getCashPurchase']);
        Route::get('/purchaseByCredit', [report::class, 'getCreditPurchase']);
        Route::get('/purchaseByLoan', [report::class, 'getLoanPurchase']);

        Route::get('/expenses', [expense::class, 'getExpenseList']);


        Route::get('brand', function () {
            return view('Inventory.brand');
        });
        Route::get('manufacturer', function () {
            return view('Inventory.manufacture');
        });
        Route::post('/brandSave', [setting::class, 'createBrand']);
        Route::post('/manufactureSave', [setting::class, 'createManufacture']);
        Route::post('/bankSave', [setting::class, 'createBank']);
        Route::post('/saveExpense', [expense::class, 'postExpense']);
    });
});
Route::get('/', function () {
    return view('auth.login');
});

Route::get('/registerUser', function () {
    return view('auth.register');
});

// Route::post('register', [RegisterController::class, 'create'])->name('register');
