<?php

namespace App\Http\Controllers\IMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\bought;
use App\Models\Items;
use Auth;
use App\Helpers\Helper;
use DB;
use Illuminate\Support\Carbon;

class dashboard extends Controller
{
    public function getDashboard()
    {
        $LendAmount = bought::where('BoughtType', 'borrow')->sum('cashAmount');

        $shopItemCount = Items::where('itemInshop', '>', 0)->count();
        $storeItemCount = Items::where('itemInStore', '>', 0)->count();
        /// this is where and   $itemRecord = Items::where([['itemInStore','>',0],['itemInshop','>',0 ]])->count();
        $itemCount = DB::table('items')
            ->where('itemInStore', '>', 0)
            ->orWhere('itemInshop', '>', 0)
            ->count();

        $itemNumber = DB::table('detailorder')->whereDate('created_at', '=', Carbon::today()->toDateString())->get()->sum("quantity");

        $totalEarnAmount = DB::table('totaltrans')->whereDate('transDate', '=', Carbon::today()->toDateString())->get()->sum("paidAmount");

        $totalEarnAmountCash = DB::table('totaltrans')->whereDate('transDate', '=', Carbon::today()->toDateString())
            ->where('PaymentMethod', 'Cash')->get()->sum("paidAmount");

        $totalEarnAmountCredit = DB::table('totaltrans')->whereDate('transDate', '=', Carbon::today()->toDateString())->where('PaymentMethod', 'Credit')->get()->sum("paidAmount");

        $totalEarnAmountMobileBanking = DB::table('totaltrans')->whereDate('transDate', '=', Carbon::today()->toDateString())->where('PaymentMethod', '>', 0)->get()->sum("paidAmount");


        return view('inventory.Dashboard', compact('itemCount', 'shopItemCount', 'storeItemCount', 'LendAmount', 'itemNumber', 'totalEarnAmount', 'totalEarnAmountCash', 'totalEarnAmountCredit', 'totalEarnAmountMobileBanking'));
    }
}
